package helper;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import entities.Dimensions;
import interfaces.OnLineReadCallback;

public class AnewHelper {
	
	private static Map<String, Dimensions> anew;
	private static String pattern = "0,00";
	private static DecimalFormat decimalFormat;
	private static DecimalFormatSymbols symbols;
	private static String anewFileLocation = "anew/anew-br.tsv";
	private static BigDecimal anewScale = new BigDecimal("9");
	private static final BigDecimal transformFactor = new BigDecimal("5");
	
	private static Map<String, Dimensions> loadANEW(){
		
		symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator(',');
		
		decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);
		
		Map<String, Dimensions> dimensionsMap = new HashMap<>();
		
		try {
			
			CSVHelper.readAllCsvLineByLine(anewFileLocation, "\t", new OnLineReadCallback() {
	
				@Override
				public void run(String[] fields) throws ParseException {
					
					BigDecimal valenceMean;
					BigDecimal valenceSD;
					BigDecimal arousalMean;
					BigDecimal arousalSD;
					
					valenceMean = toBigDecimal(fields[1]);
					valenceSD = toBigDecimal(fields[2]);
					arousalMean = toBigDecimal(fields[3]);
					arousalSD = toBigDecimal(fields[4]);
				
					dimensionsMap.put(fields[0].toLowerCase(), new Dimensions(valenceMean, valenceSD, arousalMean, arousalSD));
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
        return dimensionsMap;
	}
	
	public static Dimensions findWord(String word) {
		if (anew == null)
			anew = loadANEW();
			
		return anew.get(word.toLowerCase());
	}
	
	public static BigDecimal getWeight(String token) {
		Dimensions d = findWord(token);
		
		if (d == null) return BigDecimal.ZERO;
		
		// b = (|V - 5| * A) / 4 * 9
		BigDecimal b = d.getValenceMean().subtract(transformFactor).abs().multiply(d.getArousalMean())
				.divide(transformFactor.subtract(BigDecimal.ONE).multiply(anewScale), MathContext.DECIMAL128);
		
		return b;
	}
	
	public static Dimensions calcMean(Map<String, Dimensions> dimensions) {
		
		if (dimensions == null || dimensions.isEmpty()) return null;
		
		BigDecimal totalCount = BigDecimal.ZERO;		
		BigDecimal valenceTotal = BigDecimal.ZERO, arousalTotal = BigDecimal.ZERO;
		
		for (Entry<String, Dimensions> entry : dimensions.entrySet()) {
			
			Dimensions d = entry.getValue();
			BigDecimal count = new BigDecimal(d.getCount());			
			
			totalCount = count.add(totalCount);
			valenceTotal = valenceTotal.add(d.getValenceMean().multiply(count));
			arousalTotal = arousalTotal.add(d.getArousalMean().multiply(count));
		}
		
		BigDecimal valenceMean = valenceTotal.divide(totalCount, MathContext.DECIMAL128);
		BigDecimal arousalMean = arousalTotal.divide(totalCount, MathContext.DECIMAL128);
		
		return new Dimensions(valenceMean, BigDecimal.ZERO, arousalMean, BigDecimal.ZERO);
	}
	
	private static BigDecimal toBigDecimal(String string) throws ParseException {
		return (BigDecimal) decimalFormat.parse(string);
	}
	
}
