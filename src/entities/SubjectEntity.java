package entities;

public class SubjectEntity {
	
	private int subjectID;
	private String name;
	private String code;
	private String description;
	private boolean doneEvaluation;
	
	public SubjectEntity(int subjectID, String name, String code, boolean doneEvaluation) {
		super();
		
		this.subjectID = subjectID;
		this.name = name;
		this.code = code;
		this.doneEvaluation = doneEvaluation;
	}
	
	public int getSubjectID() {
		return subjectID;
	}
	
	public void setSubjectID(int subjectID) {
		this.subjectID = subjectID;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDoneEvaluation() {
		return doneEvaluation;
	}

	public void setDoneEvaluation(boolean doneEvaluation) {
		this.doneEvaluation = doneEvaluation;
	}
	
	
	
}
