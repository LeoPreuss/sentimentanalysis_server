package servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import db.MysqlCon;
import entities.Student;
import entities.SubjectEntity;
import entities.User;
import exceptions.LoginException;
import helper.SessionHelper;

@WebServlet("/Subject")
public class SubjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Gson gson = new Gson();
       
    public SubjectServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			
			HttpSession session = SessionHelper.validateSession(request);
							
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			
			String operation = request.getParameter("operation");
			String responseString = null;
		
			switch (operation) {
				case "list": responseString = getSubjectsFromUser((int) session.getAttribute("user_id")); break;
				case "get_students_from_subjects": responseString = getStudentsFromSubject(request); break;
				case "get_subject": responseString = getSubject(request); break;
			}
			
			response.getWriter().print(responseString);
		
		} catch (SQLException e) {
			response.getWriter().print("error");
		} catch (NumberFormatException e) {
			response.getWriter().print("invalid_parameter");
		} catch (LoginException e) {
			response.getWriter().print(e.getMessage());
		} catch (NullPointerException e) {
			response.getWriter().print("invalid_parameter");
		}
	}

	private String getSubjectsFromUser(int userID) throws SQLException, NumberFormatException {
		
		PreparedStatement pStmt = MysqlCon.getConnection().prepareStatement(
				"SELECT s.id_subject, s.name, s.code, sum(not(SELECT EXISTS (SELECT 1 FROM lesson_evaluation le WHERE le.id_lesson = sl.id_lesson AND le.id_user = us.id_user))) = 0 as done_evaluation\r\n" + 
				"FROM user_subject us\r\n" + 
				"LEFT JOIN subject_lesson sl ON us.id_subject = sl.id_subject\r\n" + 
				"LEFT JOIN lesson l ON sl.id_lesson = l.id_lesson\r\n" + 
				"JOIN subject s ON s.id_subject = us.id_subject\r\n" + 
				"WHERE us.id_user = ?\r\n" + 
				"GROUP BY us.id_subject\r\n" +
				"ORDER BY s.name");
		
		pStmt.setInt(1, userID);
		
		ResultSet rs = pStmt.executeQuery();
		
		List<SubjectEntity> subjects = new ArrayList<>();
		
		while (rs.next())
			subjects.add(new SubjectEntity(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getBoolean(4)));
		
		return new Gson().toJson(subjects);
	}
	
	private String getStudentsFromSubject(HttpServletRequest request) throws SQLException, NumberFormatException {
		
		int subjectID = Integer.parseInt(request.getParameter("subject_id"));
		int lessonID = Integer.parseInt(request.getParameter("lesson_id"));
		
		PreparedStatement pStmt = MysqlCon.getConnection().prepareStatement(
				"SELECT us.id_user, u.name, u.surname, u.email, u.type, (SELECT EXISTS (SELECT 1 FROM lesson_evaluation le WHERE le.id_lesson = ? AND le.id_user = us.id_user)) as done_evaluation\r\n" + 
				"FROM user_subject us\r\n" + 
				"JOIN users u ON u.id_users = us.id_user\r\n" + 
				"WHERE us.id_subject = ? AND u.type = 2\r\n" + 
				"ORDER BY name;");
		
		pStmt.setInt(1, lessonID);
		pStmt.setInt(2, subjectID);
		
		ResultSet rs = pStmt.executeQuery();
		
		List<User> students = new ArrayList<>();
		Student student = null;
		
		while (rs.next()){
			student = new Student(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
			student.setDoneEvaluation(rs.getBoolean(6));
			students.add(student);
		}
			
		return new Gson().toJson(students);
	}

	private String getSubject(HttpServletRequest request) throws SQLException, NumberFormatException {
		
		int subjectID = Integer.parseInt(request.getParameter("subject_id"));
		
		PreparedStatement pStmt = MysqlCon.getConnection().prepareStatement(
				"SELECT s.id_subject, s.name, s.code " + 
				"FROM subject s " + 
				"WHERE s.id_subject = ?");
		
		pStmt.setInt(1, subjectID);
		
		ResultSet rs = pStmt.executeQuery();
		SubjectEntity s = null;
		
		if (rs.next())
			s = new SubjectEntity(rs.getInt(1), rs.getString(2), rs.getString(3), false);
		
		return gson.toJson(s);
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
