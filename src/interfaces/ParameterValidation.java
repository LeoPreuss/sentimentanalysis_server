package interfaces;

public interface ParameterValidation {
	
	public void validate(String parameter) throws Exception;  
}
