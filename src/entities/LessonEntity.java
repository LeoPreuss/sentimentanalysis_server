package entities;

import java.sql.Date;
import java.sql.Timestamp;

public class LessonEntity {
	
	private int lessonID;
	private String name;
	private long start;
	private long end;
	private boolean doneEvaluation;
	
	public LessonEntity(int lessonID, String name, Timestamp start, Timestamp end, boolean doneEvaluation) {
		super();
		
		this.lessonID = lessonID;
		this.name = name;
		this.doneEvaluation = doneEvaluation;
		
		setStart(start);
		setEnd(end);
	}
	
	public int getLessonID() {
		return lessonID;
	}
	public void setLessonID(int lessonID) {
		this.lessonID = lessonID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStart() {
		return new Date(start);
	}
	public void setStart(Timestamp start) {
		this.start = start.getTime();
	}
	public Date getEnd() {
		return new Date(end);
	}
	public void setEnd(Timestamp end) {
		this.end = end.getTime();
	}

	public boolean hasDoneEvaluation() {
		return doneEvaluation;
	}

	public void setDoneEvaluation(boolean doneEvaluation) {
		this.doneEvaluation = doneEvaluation;
	}
}
