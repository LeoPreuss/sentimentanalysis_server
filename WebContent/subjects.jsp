<!DOCTYPE html>
<html>
<head>
	<%@ include file="head.jsp" %>
	<script src="res/js/subjects.js"></script>
	<title>VITA - Disciplinas</title>
</head>
  <body>
	<div class="container bg-light border rounded mt-5">
		<div class="text-center m-2">
			<h3>Disciplinas</h3>
		</div>
		<ul id="listSubject" class="list-group"></ul>
		<div class="text-center">
			<button type="button" class="btn btn-primary w-25 m-2" id="btnExit">Sair</button>
		</div>
    </div>
  </body>
</html>
