package exceptions;

public class ParamsValidationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -529054047727356723L;
	public ParamsValidationException() {}
	public ParamsValidationException(String message) {
		super(message);
	}
	
}
