package entities;

public class Student extends User {
	
	private boolean doneEvaluation;
	
	public Student(int userID, String name, String surname, String email, int type) {
		super(userID, name, surname, email, type, false);
	}
	
	public Student(int userID, String name, String surname, String email, int type, boolean defaultPassword) {
		super(userID, name, surname, email, type, defaultPassword);
	}

	public boolean hasDoneEvaluation() {
		return doneEvaluation;
	}

	public void setDoneEvaluation(boolean doneEvaluation) {
		this.doneEvaluation = doneEvaluation;
	}
	
}
