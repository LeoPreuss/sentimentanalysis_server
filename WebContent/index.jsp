<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="head.jsp" %>
	<script src="res/js/index.js"></script>	
	<title>VITA - Login</title>
</head>
  <body>
    <div class="container bg-light border rounded mt-5">
      <div class="text-center m-2">
        <h3>Login</h3>
      </div>
	  <div class="alert alert-danger" id="divMessage" style="display: none"></div>	  
      <form id="formLogin" action="Login" method="post">
		<input type="hidden" name="operation" value="login"/>
        <div class="form-group">
          <label for="login">Login: </label>
          <input type="email" name="login" class="form-control" placeholder="email@exemplo.com"/>
        </div>
        <div class="form-group">
          <label for="password">Senha: </label>
          <input name="password" class="form-control" type="password" placeholder="Senha"/>
        </div>
        <div class="text-center">
			<button id="btnLogin" type="button" class="btn btn-primary w-25 mb-3">Entrar</button>
        </div>
      </form>
    </div>
  </body>
</html>
