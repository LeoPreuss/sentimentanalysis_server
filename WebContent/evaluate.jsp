<!DOCTYPE html>
<html>
<head>
	<%@ include file="head.jsp" %>
	<script src="res/js/evaluate.js"></script>
	<title>VITA - Enviar Avalia��o</title>
	
</head>
  <body>
	<div class="container bg-light border rounded mt-5">
		<div class="text-center m-2">
			<h3>Avalia��o</h3>
		</div>
		<div class="alert alert-danger" id="divMessage" style="display: none"></div>
		<div id="divEvaluation" style="display: none">
		
			<p>Enviar uma avalia��o � muito simples, bastando apenas escrever sua opini�o sobre a aula lecionada. Como se sentiu em rela��o � aula? Lembre-se: o professor n�o ter� conhecimento de sua opini�o.</p>
	
			<form id="formEvaluate">
	
				<input type="hidden" name="lesson_id" id="inputLessonID"/>
				<input type="hidden" name="operation" value="send_evaluation"/>
				<input type="hidden" name="l" value="portuguese"/>
	
				<textarea name="t" class="form-control" rows="10"/></textarea>
				<label for="t" style="color: red">Palavras necess�rias: 70</label>
				<br/>
				
				<p style="display: inline; margin-right: 10px">De 0 a 5, qual a nota voc� d� a esta aula?</p>
				
				<select name="grade" id="selectGrade" class="form-control col-lg-1" style="display: inline">
					<option value="-1"></option>
					<option value="0">0</option>
				    <option value="1">1</option>
				    <option value="2">2</option>
				    <option value="3">3</option>
				    <option value="4">4</option>
				    <option value="5">5</option>
				</select>
				
				<div class="text-center">
					<button id="btnSendEvaluation" type="button" class="btn btn-primary w-25 m-3">Enviar</button>
				</div>
	
				<div id="divBtnBack" class="text-center" style="display: none">
					<button type="button" class="btn btn-primary w-25 m-3" onclick="window.history.back()">Voltar</button>
				</div>
			</form>
		</div>
    </div>
  </body>
</html>
