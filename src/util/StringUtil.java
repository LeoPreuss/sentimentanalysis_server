package util;

public class StringUtil {
	public static boolean isEmpty(final String s) {
	  return s == null || s.trim().isEmpty();
	}
}
