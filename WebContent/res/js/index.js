var user;

$(function(){

	$('#btnLogin').click(function(){
		doLogin();
	});
	
	var showMessage = function(message){
		$('#divMessage').show();
		$('#divMessage').html('<strong>'+ message + '</strong>');
	}
    
    var doLogin = function (){
		$('#divMessage').hide();

	    $.ajax({
        	url : "Login",
            type : 'post',
         	data : $('#formLogin').serialize(),
         	dataType: 'html',
     	 }).done(function(msg){

     	 	if (msg == "invalid_user_or_password"){
     	 		showMessage("Usuário e senha informados não foram encontrados.");
     	 		return;
     	 	}
     	 	
          	var user = JSON.parse(msg);
          	
          	if (!user.defaultPassword)          		 
          		window.location.href = "subjects.jsp?user_id=" + user.userID;
          	else
          		window.location.href = "password.jsp?user_id=" + user.userID;
          		

	     }).fail(function(jqXHR, textStatus, msg){
	          showMessage("Erro ao se comunicar com servidor.");
	     }); 
    }
});