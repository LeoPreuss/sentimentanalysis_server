$(function(){
	
	var getSubjectID = function(){
		var url = window.location.href
		var captured = /subject_id=([^&]+)/.exec(url)[1];
		return captured ? captured : 0;
	}
	
	var getCurrentSubject = function(subjectID){
		$.post("Subject", {operation: "get_subject", subject_id: subjectID}, function(data){
			var subject = JSON.parse(data);
			
			$('#title').text(subject.name);
		});
	}

	var getLessons = function (subjectID){
	        
	    $.post("Lesson", {subject_id: subjectID, operation: "list"}, function(data){

	        var lessons = JSON.parse(data);

	        $.each(lessons, function(index, value){
	        	
	        	var date = new Date(value.start).toLocaleDateString();
	        	var checked = value.doneEvaluation ? 'checked' : '';
	        	
	        	var checkBoxSection = `<div class="form-check form-check-inline"><input type="checkbox" class="form-check-input" disabled="true" ${checked}/></div>`;
	        	var html = `<a href="evaluate.jsp?lesson_id=${value.lessonID}">` + 
	        		`<li class="list-group-item" style="display: flex; justify-content: space-between;"> ${value.name}(${date}) ${checkBoxSection}</li></a>`;
	        	
	        	
	            $('#listLesson').append(html);

	        });
	    });
	}
	
	getCurrentSubject(getSubjectID())
	getLessons(getSubjectID());
	
});