$(function(){
	
	var getUserID = function(){
		var url = window.location.href
		var captured = /user_id=([^&]+)/.exec(url)[1];
		return captured ? captured : 0;
	}
	
	userID = getUserID();
	
	$('#btnChangePassword').click(function(){
		changePassword($('input[name=password]').val(), $('input[name=confirm_password]').val());
		
	});
	
	var showMessage = function(message, success){
		$('#divMessage').removeClass();
		$('#divMessage').addClass("alert");
		$('#divMessage').addClass("alert-" + (success ? "success" : "danger"));
		$('#divMessage').addClass("mt-2");
		$('#divMessage').html('<strong>'+ message + '</strong>');
		$('#divMessage').show();
	}
	
	var changePassword = function(password, confirm_password){
		
		$('#divMessage').hide();
		
		if (password != confirm_password){
			showMessage("Senhas não conferem.", false)
			return;
		}
		
		$.ajax({
        	url : "Login",
            type : 'post',
         	data : $('#formChangePassword').serialize(),
         	dataType: 'html',
     	 }).done(function(msg){
     		 
     		
			if (msg == "success"){
				window.location.href = "subjects.jsp?user_id=" + userID;
	        } else {
	        	showMessage("Erro ao alterar senha.", false);
	        }

	     }).fail(function(jqXHR, textStatus, msg){
	          showMessage("Erro ao se comunicar com servidor.");
	     });
	}
	
	
});