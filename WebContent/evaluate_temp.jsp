<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="head.jsp" %>
	<!--  <script src="res/js/evaluate.js"></script>-->
	<title>VITA - Enviar Avalia��o</title>
	
</head>
  <body>
	<div class="container bg-light border rounded mt-5">
		<div class="text-center m-2">
			<h3>P�gina Teste de Avalia��o</h3>
		</div>
		<div class="alert alert-danger" id="divMessage" style="display: none"></div>
		<div id="divEvaluation">
		
			<p>Teste de avalia��o</p>
	
			<form id="formEvaluate" action="Evaluate">
				<input type="hidden" name="operation" value="get_satisfaction_temp"/>
				<input type="hidden" name="l" value="portuguese"/>
	
				<textarea name="t" class="form-control" rows="10"/></textarea>
				<br/>
				
				<div class="text-center">
					<button id="btnSendEvaluation" type="submit" class="btn btn-primary w-25 m-3">Enviar</button>
				</div>
			</form>
		</div>
    </div>
  </body>
</html>
