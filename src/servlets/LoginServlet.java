package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import db.MysqlCon;
import entities.User;
import exceptions.LoginException;
import exceptions.ParamsValidationException;
import helper.SessionHelper;

@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson;	

    public LoginServlet() {
        super();
        gson = new Gson();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(403);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		
		String operation = request.getParameter("operation");
		
		try {
			
			String responseString = null;
			
			switch (operation) {
				case "login": responseString = login(request); break;
				case "logout": responseString = logout(request); break;
				case "change_password": responseString = changePassword(request); break;
				default: throw new IllegalArgumentException("invalid_parameter");
			}
			
			response.setContentType("application/json; charset=UTF-8");
			response.getWriter().print(responseString);
			
		} catch (SQLException e) {
			response.getWriter().print("unknown_error");
		} catch (ParamsValidationException e) {
			response.getWriter().print(e.getMessage());
		} catch (IllegalArgumentException e) {
			response.getWriter().print(e.getMessage());
		} catch (LoginException e) {
			response.getWriter().print(e.getMessage());
		}
		
	}
	/*private void carregaDadosLIWC(){

		//File file = new File("D:\\LIWC2007_Portugues_win_UTF_8.txt");
		BufferedReader reader = null;
		
		Connection conn = MysqlCon.getConnection();
			
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\LIWC2007_Portugues_win_UTF_8.txt"), "UTF-8"));
			String text = null;

			for (int i = 0; i < 65; i++) {
				 text = reader.readLine();
				 
				 if (text.contains("%"))
					 continue;
				 
				 String[] temp = text.split("\t");
				 
				 int liwcID = Integer.parseInt(temp[0]);
				 
				 String categoryName = temp[1];
				 
				 try {
					PreparedStatement pStmt = conn.prepareStatement("INSERT INTO category (liwc_id, name) VALUES (?, ?)");
					
					pStmt.setInt(1, liwcID);
					pStmt.setString(2, categoryName);
					pStmt.executeUpdate();
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
	    	} catch (IOException e) {
				e.printStackTrace();
	    	}
		}

	}*/

	private String changePassword(HttpServletRequest request) throws SQLException, LoginException, IllegalArgumentException {
		
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirm_password");
			
		HttpSession session = SessionHelper.validateSession(request);
	
		Connection conn = MysqlCon.getConnection();
		
		if (password != null && password.trim().length() > 0 && password.equals(confirmPassword)) {
			
			String passwordHash = org.apache.commons.codec.digest.DigestUtils.sha256Hex(password);
			
			PreparedStatement pStmt = conn.prepareStatement("UPDATE users u SET u.password = ?, u.default_password = ? WHERE u.id_users = ?");
			
			pStmt.setString(1, passwordHash);
			pStmt.setBoolean(2, false);
			pStmt.setInt(3, (int) session.getAttribute("user_id"));
			
			pStmt.executeUpdate();
			
			return "success";
		}
		
		throw new IllegalArgumentException("passwords_do_not_match");
	}

	private String logout(HttpServletRequest request) throws LoginException {
		
		HttpSession session = SessionHelper.validateSession(request);
		
		session.invalidate();
		
		return "success";
	}

	private String login(HttpServletRequest request) throws SQLException, ParamsValidationException {
		Connection conn = MysqlCon.getConnection();
		
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		if (login == null || password == null)
			throw new ParamsValidationException("invalid_parameter");
			
		String passwordHash = org.apache.commons.codec.digest.DigestUtils.sha256Hex(password);
		
		PreparedStatement pStmt = conn.prepareStatement("SELECT * FROM users u WHERE u.email = ? AND u.password = ?");
		pStmt.setString(1, login);
		pStmt.setString(2, passwordHash);
		
		ResultSet rs = pStmt.executeQuery();
		
		String returnString;
		
		if (rs.last()) {
			HttpSession session = request.getSession(true);
			User user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(7), rs.getBoolean(8));
			
			session.setAttribute("user_id", user.getUserID());
			session.setAttribute("user_type", user.getType());
			
			returnString = gson.toJson(user);
			
		} else {
			returnString = "invalid_user_or_password";
		}
		
		pStmt.close();
		return returnString;
	}
}
