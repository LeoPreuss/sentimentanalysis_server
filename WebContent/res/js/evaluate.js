$(function(){
		
	var minWords = 70;
	var remainingWords = minWords;
		
	var getCurrentLessonID = function(){
		var url = window.location.href
		var captured = /lesson_id=([^&]+)/.exec(url)[1];
		return captured ? captured : 0;
	}

	var showMessage = function(message, success){
		$('#divMessage').removeClass();
		$('#divMessage').addClass("alert");
		$('#divMessage').addClass("alert-" + (success ? "success" : "danger"));
		$('#divMessage').addClass("mt-2");
		$('#divMessage').html('<strong>'+ message + '</strong>');
		$('#divMessage').show();
	}

	$('#inputLessonID').val(getCurrentLessonID());

	var sendEvaluation = function (){
		
		refreshRemainingWords();
		
		if (remainingWords > 0){
			showMessage("Número insuficientes de palavras. ", false);
			return;
		}
		
		if ($('#selectGrade option:selected').val() === "-1"){
			showMessage("É necessário informar a nota da aula", false);
			return;
		}
		
		$('#divMessage').hide();
		$('#btnSendEvaluation').prop('disabled', true);
		
	    $.ajax({
        	url : "Evaluate",
            type : 'post',
         	data : $('#formEvaluate').serialize(),
         	dataType: 'html',
     	 }).done(function(msg){
			if (msg == "success"){
	        	showMessage("Avaliação enviada com sucesso.", true);
	        	$('#divBtnBack').show();
	        	$('#btnSendEvaluation').hide();
	        } else {
	        	showMessage("Erro ao enviar avaliação.", false);
	        	$('#btnSendEvaluation').prop('disabled', false);
	        }

	     }).fail(function(jqXHR, textStatus, msg){
	          showMessage("Erro ao se comunicar com servidor.");
	          $('#btnSendEvaluation').prop('disabled', false);
	     });
	    
	}
	
	var refreshRemainingWords = function(){
		
		var matches = $("textarea[name=t]").val().match(/\S+/g);
		var textLength = matches != null ? matches.length : 0;
		
		remainingWords = minWords - textLength;

	    $('label[for=t]').text(`Palavras necessárias: ${remainingWords} `.concat(remainingWords > 0 ? '' : '(OK)'));
	    $('label[for=t]').css('color', remainingWords > 0 ? 'red' : 'green');
	}
	
	var checkEvaluationExists = function (){

		$('#divMessage').hide();
	    $.ajax({
        	url : "Lesson",
            type : 'post',
         	data : {operation: "check_evaluation_exist", lesson_id: $('#inputLessonID').val()},
         	dataType: 'html',
     	 }).done(function(msg){
			if (msg == "false"){
				$('#divEvaluation').show();
				
				$('#btnSendEvaluation').click(function(){
					sendEvaluation();
				});
				
				$("textarea[name=t]").on('input', function(){
				    refreshRemainingWords();
				});
	        } else {
	        	showMessage("Avaliação já realizada.", false);
	        }

	     }).fail(function(jqXHR, textStatus, msg){
	          showMessage("Erro ao se comunicar com servidor.");
	     });
	}
	
	checkEvaluationExists();
});