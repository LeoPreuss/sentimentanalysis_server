package servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import db.MysqlCon;
import entities.LessonEntity;
import exceptions.LoginException;
import helper.SessionHelper;

@WebServlet("/Lesson")
public class LessonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LessonServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		
		try {
			
			HttpSession session = SessionHelper.validateSession(request);
			
			int userID = (int) session.getAttribute("user_id");
			
			String operation = request.getParameter("operation");
			
			switch (operation) {
				case "list": response.getWriter().print(listStudentLessons(request, (int) session.getAttribute("user_id"))); break;
				case "check_evaluation_exist": {
					
					String studentID = request.getParameter("student_id");
					
					if (studentID != null)
						response.getWriter().print(checkEvaluationAlreadyExists(request, Integer.parseInt(studentID)));
					else
						response.getWriter().print(checkEvaluationAlreadyExists(request, userID));
					break;
				}
			}
		
		} catch (SQLException e) {
			response.getWriter().print("error");
		} catch (NumberFormatException e) {
			response.getWriter().print("invalid_parameter");
		} catch (NullPointerException e) {
			response.getWriter().print("invalid_parameter");
		} catch (LoginException e) {
			response.getWriter().print(e.getMessage());
		}
	}
	
	private String listStudentLessons(HttpServletRequest request, int userID) throws NumberFormatException, SQLException {
		return getStudentLessons(Integer.parseInt(request.getParameter("subject_id")), userID);
	}

	private String getStudentLessons(int subjectID, int userID) throws SQLException {
		
		PreparedStatement pStmt = MysqlCon.getConnection().prepareStatement(
				"SELECT l.id_lesson, l.name, l.start, l.end, (SELECT EXISTS (SELECT 1 FROM lesson_evaluation le WHERE le.id_lesson = sl.id_lesson AND le.id_user = us.id_user)) as done_evaluation\r\n" + 
				"FROM subject_lesson sl\r\n" + 
				"JOIN lesson l ON sl.id_lesson = l.id_lesson\r\n" + 
				"JOIN user_subject us ON us.id_subject = sl.id_subject\r\n" + 
				"WHERE sl.id_subject = ? AND us.id_user = ?\r\n" +
				"ORDER BY l.start;");
		
		pStmt.setInt(1, subjectID);
		pStmt.setInt(2, userID);
		
		ResultSet rs = pStmt.executeQuery();
		
		List<LessonEntity> lessons = new ArrayList<>();
		
		while (rs.next())
			lessons.add(new LessonEntity(rs.getInt(1), rs.getString(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getBoolean(5)));
		
		return new Gson().toJson(lessons);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	private boolean checkEvaluationAlreadyExists(HttpServletRequest request, int userID) throws NumberFormatException, SQLException {
		
		int lessonID = Integer.parseInt(request.getParameter("lesson_id"));
		
		PreparedStatement pStmt = MysqlCon.getConnection().prepareStatement("SELECT EXISTS (SELECT 1 FROM lesson_evaluation le WHERE le.id_lesson = ? AND le.id_user = ?) as c");
		pStmt.setInt(1, lessonID);
		pStmt.setInt(2, userID);
		
		ResultSet rs = pStmt.executeQuery();
		
		rs.next();
		
		return rs.getInt("c") > 0;
	}

}
