<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<%@ include file="head.jsp" %>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="res/js/password.js"></script>
		<title>Trocar senha</title>
	</head>
	<body>
		<div class="container bg-light border rounded mt-5">
			<div class="text-center m-2">
				<h3> Alterar senha padrão</h3>
			</div>

			<div class="alert alert-danger" id="divMessage" style="display: none"></div>
		
			<form id="formChangePassword" method="post">
				<input type="hidden" name="operation" value="change_password">
				<div class="form-group">
			       <label for="login">Nova senha: </label>
			       <input type="password" name="password" class="form-control"/>
		        </div>
						
				<div class="form-group">
			         <label for="login">Confirme nova senha: </label>
			         <input type="password" name="confirm_password" class="form-control"/>
		        </div>
			
				<div class="text-center">
					<button id="btnChangePassword" type="button" class="btn btn-primary w-25 mb-3">Alterar senha</button>
		       	</div>
        	</form>
		</div>

	</body>
</html>