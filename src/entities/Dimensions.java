package entities;

import java.math.BigDecimal;

public class Dimensions{
	private BigDecimal valenceMean;
	private BigDecimal valenceSD;
	private BigDecimal arousalMean;
	private BigDecimal arousalSD;
	private int count;

	public BigDecimal getValenceMean() {
		return valenceMean;
	}

	public void setValenceMean(BigDecimal valenceMean) {
		this.valenceMean = valenceMean;
	}

	public BigDecimal getValenceSD() {
		return valenceSD;
	}

	public void setValenceSD(BigDecimal valenceSD) {
		this.valenceSD = valenceSD;
	}

	public BigDecimal getArousalMean() {
		return arousalMean;
	}

	public void setArousalMean(BigDecimal arousalMean) {
		this.arousalMean = arousalMean;
	}

	public BigDecimal getArousalSD() {
		return arousalSD;
	}

	public void setArousalSD(BigDecimal arousalSD) {
		this.arousalSD = arousalSD;
	}

	public Dimensions (BigDecimal valenceMean, BigDecimal valenceSD, BigDecimal arousalMean, BigDecimal arousalSD){
		this.valenceMean = valenceMean;
		this.valenceSD = valenceSD;
		this.arousalMean = arousalMean;
		this.arousalSD = arousalSD;
		this.count = 1;
	}

	public void IncCount() {
		count++;		
	}
	
	public int getCount() {
		return count;
	}
}
