<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	
	<%@ include file="head.jsp" %>
	<script src="res/js/lesson.js"></script>
	<title>VITA - Aulas</title>
</head>
  <body>
	<div class="container bg-light border rounded mt-5">
		<div class="text-center m-2">
			<h3 id="title">Aulas</h3>
		</div>
		<ul id="listLesson" class="list-group"></ul>
		<div class="text-center">
			<button type="button" class="btn btn-primary w-25 m-2" onclick="window.history.back()">Voltar</button>
		</div>
    </div>
  </body>
</html>
