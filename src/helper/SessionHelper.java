package helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import exceptions.LoginException;

public class SessionHelper {
	
	public static HttpSession validateSession (HttpServletRequest request) throws LoginException {
		HttpSession session = request.getSession(false);
		
		if (session == null)
			throw new LoginException("not_logged_yet");
		
		return session;
	}

}
