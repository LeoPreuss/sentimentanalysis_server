package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.MysqlCon;
import exceptions.ParamsValidationException;
import global.Globals;
import util.StringUtil;

@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RegisterServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(403);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirm_password");
		
		try {
			
			validateRequestParams(name, surname, email, password, confirmPassword);
			
			String passwordHash = org.apache.commons.codec.digest.DigestUtils.sha256Hex(password);
			
			Connection conn = MysqlCon.getConnection();
			PreparedStatement pStmt = conn.prepareStatement("INSERT INTO users (name, surname, email, password) VALUES (?, ?, ?, ?)");
			pStmt.setString(1, name);
			pStmt.setString(2, surname);
			pStmt.setString(3, email);
			pStmt.setString(4, passwordHash);
			
			pStmt.executeUpdate();
			response.getWriter().print("success");
			
		} catch (SQLException e) {
			response.getWriter().print("unknown_error");
			e.printStackTrace();
		} catch (ParamsValidationException e) {
			response.getWriter().print(e.getMessage());
		}
	}

	private void validateRequestParams(String name, String surname, String email, String password, String confirmPassword) throws ParamsValidationException {
		
		if (StringUtil.isEmpty(name) || StringUtil.isEmpty(surname) || StringUtil.isEmpty(email) || StringUtil.isEmpty(password) || StringUtil.isEmpty(confirmPassword))
			throw new ParamsValidationException("empty_fields");
		
		if (password.trim().length() < Globals.PASSWORD_MIN_LENGTH)
			throw new ParamsValidationException("password_too_small");
		
		if (password.trim().length() > Globals.PASSWORD_MAX_LENGTH)
			throw new ParamsValidationException("password_too_large");
		
		if (!password.equals(confirmPassword))
			throw new ParamsValidationException("passwords_do_not_match");
	}

}
