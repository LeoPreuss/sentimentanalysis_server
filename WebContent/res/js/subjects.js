$(function(){
	
	var getCurrentUserID = function(){
		var url = window.location.href
		var captured = /user_id=([^&]+)/.exec(url)[1];
		return captured ? captured : 0;
	}
	
	$('#btnExit').click(function(){
		doLogout();
	});

	var getSubjects = function (){
       
        $.post("Subject", {operation: "list"}, function(data){

            var lessons = JSON.parse(data);

            $.each(lessons, function(index, value){
                $('#listSubject')
                   .append('<a href="lesson.jsp?subject_id=' + value.subjectID + '"><li class="list-group-item">' + value.name + '</li></a>');

            });
        });
    }
	
	var doLogout = function (){
		$('#divMessage').hide();

	    $.ajax({
        	url : "Login",
            type : 'post',
         	data : {operation: 'logout'},
         	dataType: 'html',
     	 }).done(function(msg){

         	window.history.back();

	     }).fail(function(jqXHR, textStatus, msg){
	          showMessage("Erro ao se comunicar com servidor.");
	     }); 
    }

    getSubjects();
});