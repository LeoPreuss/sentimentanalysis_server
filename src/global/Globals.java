package global;

import java.math.BigDecimal;

public class Globals {
	
	public static final int PASSWORD_MIN_LENGTH = 6;
	public static final int PASSWORD_MAX_LENGTH = 30;
	public static final String AnewPath = null;
	public static final BigDecimal AnewWeight = new BigDecimal("0.8");
	public static final BigDecimal LiwcWeight = BigDecimal.ONE.subtract(AnewWeight);

}