package helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import interfaces.OnLineReadCallback;

public class CSVHelper {
	
	public static void readAllCsvLineByLine(String filePath) throws Exception  {
		readAllCsvLineByLine(filePath, ",", null);
	}
	
	public static void readAllCsvLineByLine(String filePath, OnLineReadCallback callWhenReading) throws Exception {
		readAllCsvLineByLine(filePath, ",", callWhenReading);
	}
	
	public static void readAllCsvLineByLine(String filePath, String separator, OnLineReadCallback callWhenReading) throws Exception {
		
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath);
		
		BufferedReader in;
		try {
			in = new BufferedReader(new InputStreamReader(input, "UTF8"));
			
			String line;
			
				while ((line = in.readLine()) != null) {
					String[] parts = line.split(separator);
					
					if (callWhenReading != null)
						callWhenReading.run(parts);
				}				
            in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
