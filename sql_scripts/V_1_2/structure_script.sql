ALTER TABLE `sentanalysis`.`lesson_user_text` 
ADD COLUMN `grade` INT NULL AFTER `text`;

ALTER TABLE `sentanalysis`.`lesson_user_text` 
ADD COLUMN `pre_teacher_opinion_satisfaction` INT NULL AFTER `grade`,
ADD COLUMN `pre_teacher_opinion_confidence` INT NULL AFTER `pre_teacher_opinion_satisfaction`,
ADD COLUMN `post_teacher_opinion_satisfaction` INT NULL AFTER `pre_teacher_opinion_confidence`,
ADD COLUMN `post_teacher_opinion_confidence` INT NULL AFTER `post_teacher_opinion_satisfaction`;

ALTER TABLE lesson_user_text
ADD UNIQUE `unique_id_user_id_lesson`(`id_user`, `id_lesson`);