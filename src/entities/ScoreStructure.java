package entities;

import java.math.BigDecimal;
import global.Globals;

public class ScoreStructure {
	private BigDecimal liwcScore;
	private BigDecimal anewScore;
	
	public ScoreStructure(BigDecimal liwcCount, BigDecimal anewScore) {
		this.liwcScore = liwcCount;
		this.anewScore = anewScore;
	}
	
	public BigDecimal getLiwcCount() {
		return liwcScore;
	}
	public void setLiwcCount(BigDecimal liwcScore) {
		this.liwcScore = liwcScore;
	}
	public BigDecimal getAnewScore() {
		return anewScore;
	}
	public void setAnewScore(BigDecimal anewScore) {
		this.anewScore = anewScore;
	}
		
	public void sumAnewScore(BigDecimal increment) {
		anewScore = anewScore.add(increment);
	}
	
	public void sumLiwcCount(BigDecimal increment) {
		liwcScore = liwcScore.add(increment);
	}
	
	// (LiwcCount * LiwcWeight) + (AnewScore * AnewWeight)
	public BigDecimal getLiwcAnScore() {
		return liwcScore.multiply(Globals.LiwcWeight).add(anewScore.multiply(Globals.AnewWeight));
	}

}
