package helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import com.liwc.core.dictionary.DicFile;

public class LiwcHelper {
	
	private DicFile dic;
	
	public LiwcHelper(DicFile dic) {
		this.dic = dic;
	}
	
	public List<Integer> fromStringToIntCategory(String[] desiredCategoriesString) {
		return fromStringToIntCategory(java.util.Arrays.asList(desiredCategoriesString));
	}

	public List<Integer> fromStringToIntCategory(List<String> desiredCategoriesString) {
		List<Integer> desiredCategories = new ArrayList<>();
		
		for (String c : desiredCategoriesString){
			
			@SuppressWarnings("unchecked")
			Integer i = (Integer) getKeyByValue(dic.getCategoriesPlain(), c);
			
			if (i != null)
				desiredCategories.add(i);
		}
		
		return desiredCategories;
	}
	
	public boolean belongsToCategory(String word, String category) {
		return dic.findWord(word) != null && dic.findWord(word).contains(fromStringToIntCategory(category));
	}
	
	@SuppressWarnings("unchecked")
	public Integer fromStringToIntCategory(String category) {
		return (Integer) getKeyByValue(dic.getCategoriesPlain(), category);
	}
	
	private <T, E> T getKeyByValue(Map<T, E> map, E value) {
	    for (Entry<T, E> entry : map.entrySet())
	        if (Objects.equals(value, entry.getValue()))
	            return entry.getKey();
	    
	    return null;
	}
	

}
