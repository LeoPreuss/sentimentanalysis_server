package interfaces;

public interface OnLineReadCallback {
	public void run(String[] fields) throws Exception;
}
