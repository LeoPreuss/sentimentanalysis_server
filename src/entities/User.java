package entities;


public class User {
	
	private int userID;
	private String name;
	private String surname;
	private String email;
	private int type;
	private boolean defaultPassword;
	
	public User(int userID, String name, String surname, String email, int type) {
		this(userID, name, surname, email, type, false);
	}
	
	public User(int userID, String name, String surname, String email, int type, boolean defaultPassword) {
		super();
		this.userID = userID;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.defaultPassword = defaultPassword;
		setType(type);
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public UserType getType() {
		return UserType.values()[type - 1];
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public void setType(UserType type) {
		this.type = type.ordinal();
	}
	
	public void setDefaultPassword(boolean defaultPassword) {
		this.defaultPassword = defaultPassword;
	}

	public boolean isDefaultPassword() {
		return defaultPassword;
	}
	
}
