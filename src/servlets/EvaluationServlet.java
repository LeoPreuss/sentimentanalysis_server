package servlets;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.liwc.core.dictionary.DicFile;
import com.mysql.jdbc.Statement;

import db.MysqlCon;
import entities.ScoreStructure;
import entities.UserType;
import exceptions.LoginException;
import helper.AnewHelper;
import helper.LiwcHelper;
import helper.SessionHelper;

@WebServlet("/Evaluate")
public class EvaluationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private DicFile dic;
	private DicFile englishDic;
	private DicFile portugueseDic;
	private static final Gson gson = new Gson();
	private static final String[] negemoSubCategoriesString = {"anx", "anger", "sad"};
	private static List<Integer> negemoSubCategories;
	private static final String[] affectiveCategoriesString = {"anx", "anger", "sad", "posemo", "negemo"};
	private static List<Integer> affectiveCategories;
	private static LiwcHelper liwcHelper;
	
	DecimalFormat df;

    public EvaluationServlet() {
        super();
        
        initializeLiwc();
		df = new DecimalFormat("#.00");
		
		negemoSubCategories = liwcHelper.fromStringToIntCategory(negemoSubCategoriesString);
		affectiveCategories = liwcHelper.fromStringToIntCategory(affectiveCategoriesString);
    }

	private void initializeLiwc() {
		String classpathLocation = "liwc/LIWC2007_Portugues_win_UTF_8.txt";
		
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(classpathLocation);
		
		portugueseDic = new DicFile(input);
		//englishDic = new DicFile(new FileInputStream("E:\\LIWC2015_English.dic"));
		
		dic = portugueseDic;
		liwcHelper = new LiwcHelper(dic);
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			
			HttpSession session = SessionHelper.validateSession(request);
			
			int userID = (int) session.getAttribute("user_id");
			
			String operation = request.getParameter("operation");
			String responseString = null;
			
			switch (operation) {
				case "send_evaluation": {
					
					UserType userType = (UserType) request.getSession().getAttribute("user_type");
					
					if (userType == UserType.Student) {
						responseString = sendEvaluation(request, userID);
					} else if (userType == UserType.Teacher)
						responseString = sendTeacherEvaluation(request);
					break;
				} case "get_evaluation" : responseString = getEvaluation(request); break;
				case "get_evaluation_classroom": responseString = getEvaluationClassroom(request); break;
				case "get_satisfaction": responseString = getSatisfaction(request); break;
				case "get_satisfaction_classroom": responseString = getSatisfactionClassroom(request); break;
				case "get_satisfaction_temp": responseString = getTempSatisfaction((String) request.getParameter("t")); break;
				default: throw new IllegalArgumentException("invalid_parameter");
			}
			
			response.setContentType("application/json; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(responseString);
		
		} catch (NumberFormatException e) {
			response.getWriter().print("invalid_parameter");
		} catch (IllegalArgumentException e) {
			response.getWriter().print(e.getMessage());
		} catch (SQLException e) {
			response.getWriter().print("unknown_error");
		} catch (NullPointerException e) {
			response.getWriter().print("invalid_parameter");
		} catch (LoginException e) {
			response.getWriter().print(e.getMessage());
		}
		
	}

	private String getSatisfaction(HttpServletRequest request) throws SQLException {
		
		ResultSet rs = queryEvaluation(request);
		
		return calcSatisfaction(calcLIWCAnScore(rs)).toString();
	}

	private String getSatisfactionClassroom(HttpServletRequest request) throws SQLException {
		
		ResultSet rs = queryEvaluationClassroom(request);
		
		return calcSatisfaction(calcLIWCAnScore(rs)).toString();
	}

	private String getEvaluationClassroom(HttpServletRequest request) throws SQLException, IllegalArgumentException, NumberFormatException {
		
		ResultSet rs = queryEvaluationClassroom(request);
		
		return gson.toJson(calcLIWCAnScore(rs).entrySet()
		        .stream()
		        .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
		        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new)));
	}
	
	private ResultSet queryEvaluationClassroom(HttpServletRequest request) throws SQLException {
		int lessonID = Integer.parseInt(request.getParameter("lesson_id"));
		
		boolean onlyDesiredCategories = false;
		
		List<String> desiredCategoriesString = new ArrayList<>();

		if (request.getParameterValues("c[]") != null){
			desiredCategoriesString = Arrays.asList(request.getParameterValues("c[]"));
			onlyDesiredCategories = true;
		}
		
		String query = "SELECT c.name, sum(ec.count), sum(ec.anew_score)\r\n" + 
				"FROM evaluation_category ec\r\n" + 
				"JOIN lesson_evaluation le ON ec.id_evaluation = le.id_evaluation\r\n" + 
				"JOIN category c ON c.id_category = ec.id_category\r\n" + 
				"JOIN subject_lesson sl ON sl.id_lesson = le.id_lesson\r\n" + 
				"WHERE le.id_lesson = ? ";
				
		if (onlyDesiredCategories)
			query = query.concat("AND c.liwc_id IN " + generateQueryArrayString(liwcHelper.fromStringToIntCategory(desiredCategoriesString)));
		
		query += " GROUP BY name";
		
		PreparedStatement pStmt = MysqlCon.getConnection().prepareStatement(query);
		pStmt.setInt(1, lessonID);
		
		ResultSet rs = pStmt.executeQuery();
		return rs;
	}
	
	private String[] tokenize(String sentence) {
		return sentence.split("\\P{L}+");
	}
	
	private String sendTeacherEvaluation(HttpServletRequest request) throws IllegalArgumentException, NumberFormatException, SQLException, ServletException, IOException {
		Connection conn = MysqlCon.getConnection();
		
		int preSatisfaction = Integer.parseInt(request.getParameter("pre_satisfaction"));
		int preConfidence = Integer.parseInt(request.getParameter("pre_confidence"));
		int postSatisfaction = Integer.parseInt(request.getParameter("post_satisfaction"));
		int postConfidence = Integer.parseInt(request.getParameter("post_confidence"));
		int studentID = Integer.parseInt(request.getParameter("student_id"));
		int lessonID = Integer.parseInt(request.getParameter("lesson_id"));
		
		try {
			PreparedStatement pStmt;
			
			pStmt = conn.prepareStatement("UPDATE lesson_user_text lut "
					+ "SET pre_teacher_opinion_satisfaction = ?, "
					+ "pre_teacher_opinion_confidence = ?, "
					+ "post_teacher_opinion_satisfaction = ?, "
					+ "post_teacher_opinion_confidence = ? "
					+ "WHERE lut.id_lesson = ? AND lut.id_user = ?");
			
			pStmt.setInt(1, preSatisfaction);
			pStmt.setInt(2, preConfidence);
			pStmt.setInt(3, postSatisfaction);
			pStmt.setInt(4, postConfidence);
			pStmt.setInt(5, lessonID);
			pStmt.setInt(6, studentID);
			
			if (pStmt.executeUpdate() > 0) {
				conn.commit();
				return "success";
			}
			
			return "error";
		
		} catch (SQLException e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String sendEvaluation(HttpServletRequest request, int userID) throws IllegalArgumentException, NumberFormatException, SQLException, ServletException, IOException {
		
		String sentence = request.getParameter("t");
		String language = request.getParameter("l");
		int lessonID = Integer.parseInt(request.getParameter("lesson_id"));
		int grade = Integer.parseInt(request.getParameter("grade"));
		
		Map<Integer, ScoreStructure> liwcAnScores = getLiwcAnStructure(sentence, language);
		
		Connection conn = MysqlCon.getConnection();
		
		try {
			
			conn.setAutoCommit(false); //Inicia uma transação
			PreparedStatement pStmt;
						
			for (Map.Entry<Integer, ScoreStructure> mapEntry : liwcAnScores.entrySet()) {
				
				int categoryLiwcID = mapEntry.getKey(), categoryID;
				int quantity = mapEntry.getValue().getLiwcCount().intValue();
				
				pStmt = conn.prepareStatement("SELECT id_category FROM category c WHERE c.liwc_id = ?");
				pStmt.setInt(1, categoryLiwcID);
				
				ResultSet rs = pStmt.executeQuery();
				rs.first();
				categoryID = rs.getInt(1);
				
				pStmt = conn.prepareStatement("INSERT INTO evaluation_category (id_category, count, anew_score) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
				
				pStmt.setInt(1, categoryID);
				pStmt.setInt(2, quantity);
				pStmt.setBigDecimal(3, liwcAnScores.get(mapEntry.getKey()).getAnewScore());
				pStmt.executeUpdate();
				
				ResultSet rsEvaluationID = pStmt.getGeneratedKeys();
				rsEvaluationID.first();
				int evaluationID = rsEvaluationID.getInt(1);
				
				pStmt = conn.prepareStatement("INSERT INTO lesson_evaluation (id_lesson, id_user, id_evaluation) VALUES (?, ?, ?)");
				
				pStmt.setInt(1, lessonID);
				pStmt.setInt(2, userID);
				pStmt.setInt(3, evaluationID);
				
				pStmt.executeUpdate();
			}
			
			pStmt = conn.prepareStatement("INSERT INTO lesson_user_text(id_lesson, id_user, text, grade) VALUES (?, ?, ?, ?)");
			
			pStmt.setInt(1, lessonID);
			pStmt.setInt(2, userID);
			pStmt.setString(3, sentence);
			pStmt.setInt(4, grade);
			
			pStmt.executeUpdate();
			
			conn.commit(); //Do commit
			conn.setAutoCommit(true); // Terminate transaction
			
			return "success";
		
		} catch (SQLException e) {
			conn.rollback();
			throw e;
		}
	}

	private Map<Integer, ScoreStructure> getLiwcAnStructure(String sentence, String language) {
		Map<Integer, ScoreStructure> scoreStructures = new HashMap<>();
		
		if (sentence == null || language == null)
			throw new IllegalArgumentException("invalid_parameter");
		
		changeDictionaryLanguage(language);
		
		String[] wordsArray = tokenize(sentence);
				
		int wordPosition = -1;
		for (String word : wordsArray){
			wordPosition++;
			@SuppressWarnings("unchecked")
			List<Integer> categories = dic.findWord(word);
			
			if (categories == null)
				continue;
			
			boolean hasNegemoSubcategory = !Collections.disjoint(categories, negemoSubCategories);
			boolean hasShifterWord = hasShifterWord(wordsArray, wordPosition);
			BigDecimal anewWeight = AnewHelper.getWeight(word);
			
			for (Integer category : categories) {
				if (hasNegemoSubcategory && isNegEmo(category))
					continue; // Prevent adding twice negative sub-categories
				
				if (hasShifterWord) //Invert negative and positive categories
					if (affectiveCategories.contains(category)) {
						if (isPositiveWord(word))
							category = liwcHelper.fromStringToIntCategory("negemo");
						if (isNegativeWord(word))
							category = liwcHelper.fromStringToIntCategory("posemo");
					}
					
				if (scoreStructures.putIfAbsent(category, new ScoreStructure(BigDecimal.ONE, anewWeight)) != null) {
					ScoreStructure currentScoreStructure = scoreStructures.get(category);
					
					currentScoreStructure.sumLiwcCount(BigDecimal.ONE);
					currentScoreStructure.sumAnewScore(anewWeight);
				}
			}
		}
		
		return scoreStructures;
		
	}
	
	private boolean hasShifterWord(String[] wordsArray, int position) {
		if (position == 0)
			return false;
		
		if (!(isNegativeWord(wordsArray[position]) || isPositiveWord(wordsArray[position])))
			return false;
		
		if (position == 1)
			return isNegationWord(wordsArray[position - 1]);
		
		boolean immediateNegation = isNegationWord(wordsArray[position - 1]) || isNegationWord(wordsArray[position - 2]);
		
		if (immediateNegation)
			return true; 
		
		int verbPos = findNextVerb(wordsArray, position);
		if (verbPos > 1)
			return isNegationWord(wordsArray[verbPos - 1]) || isNegationWord(wordsArray[verbPos - 2]);
		else if (verbPos > 0)
			return isNegationWord(wordsArray[verbPos - 1]);
		else
			return false;
	}
	
	private int findNextVerb(String[] wordsArray, int position) {
		
		final int maxVerbDistance = 4;
		
		for (int i = position - 1; i >= Math.max(0, (position - maxVerbDistance)); i--) {
			if (isVerb(wordsArray[i]))
				return i;
		}
		
		return -1;
	}
	
	
	private boolean isVerb(String word) {
		return liwcHelper.belongsToCategory(word, "verb");
	}
	
	private boolean isNegativeWord(String word) {
		return liwcHelper.belongsToCategory(word, "negemo") ||
		liwcHelper.belongsToCategory(word, "anx") ||
		liwcHelper.belongsToCategory(word, "sad") ||
		liwcHelper.belongsToCategory(word, "anger");
	}

	private boolean isNegEmo(Integer category) {
		return category.equals(liwcHelper.fromStringToIntCategory("negemo"));
	}
	
	private boolean isPositiveWord(String word) {
		return liwcHelper.belongsToCategory(word, "posemo");
	}
	
	private boolean isNegationWord(String word) {
		return liwcHelper.belongsToCategory(word, "negate");
	}
	
	private Map<String, BigDecimal> calcLIWCAnScore(Map<Integer, ScoreStructure> mapScores) throws SQLException {
		
		Map<String, BigDecimal> mapCategories = new HashMap<>();
		BigDecimal totalSumm = BigDecimal.ZERO, score = BigDecimal.ZERO;
		
		for (Map.Entry<Integer, ScoreStructure> entry : mapScores.entrySet()) {
			score = entry.getValue().getLiwcAnScore();
			
			mapCategories.put((String) dic.getCategoriesPlain().get(entry.getKey()) , score);
			totalSumm = totalSumm.add(entry.getValue().getLiwcAnScore());
		}
		
		return mapCategories;
	}

	private Map<String, BigDecimal> calcLIWCAnScore(ResultSet rs) throws SQLException {
		
		Map<String, BigDecimal> mapCategories = new HashMap<>();
		BigDecimal totalSumm = BigDecimal.ZERO, anewScore = BigDecimal.ZERO, liwcScore = BigDecimal.ZERO;

		while (rs.next()) {
			anewScore = rs.getBigDecimal(3) != null ? anewScore = rs.getBigDecimal(3) : BigDecimal.ZERO;
			liwcScore = rs.getBigDecimal(2);
			
			BigDecimal score = new ScoreStructure(liwcScore, anewScore).getLiwcAnScore();
						
			totalSumm = totalSumm.add(score);
			mapCategories.put(rs.getString(1), score);
		}
		
		return mapCategories;
	}

	@SuppressWarnings("unused")
	private Map<String, BigDecimal> fromScoresToProbabilities(Map<String, BigDecimal> mapCategories, BigDecimal totalSumm) {
		Map<String, BigDecimal> mapCategoriesProbabilities = new HashMap<>();
		
		for (Map.Entry<String, BigDecimal> mapEntry : mapCategories.entrySet()) {
			BigDecimal percentual = mapEntry.getValue().divide(totalSumm, MathContext.DECIMAL128).multiply(new BigDecimal(100));
			mapCategoriesProbabilities.put(mapEntry.getKey(), percentual);
		}
		
		return mapCategoriesProbabilities;
	}
	
	
	private Map<String, BigDecimal> calcSatisfaction(Map<String, BigDecimal> map) {
		
		BigDecimal satisfaction = map.containsKey("posemo") ? map.get("posemo") : BigDecimal.ZERO;
		
		BigDecimal negemo = map.containsKey("negemo") ? map.get("negemo") : BigDecimal.ZERO;
		BigDecimal anx = map.containsKey("anx") ? map.get("anx") : BigDecimal.ZERO;
		BigDecimal sad = map.containsKey("sad") ? map.get("sad") : BigDecimal.ZERO;
		BigDecimal anger = map.containsKey("anger") ? map.get("anger") : BigDecimal.ZERO;
		BigDecimal dissatisfaction = negemo.add(anx).add(sad).add(anger);
				
		Map<String, BigDecimal> mapSatisfaction = new HashMap<>();
		
		if (satisfaction.equals(BigDecimal.ZERO) && dissatisfaction.equals(BigDecimal.ZERO))
			return mapSatisfaction;
		
		satisfaction = satisfaction.divide(satisfaction.add(dissatisfaction), MathContext.DECIMAL128).multiply(new BigDecimal("5")).setScale(1, RoundingMode.HALF_UP);
		
		mapSatisfaction.put("Satisfação", satisfaction);
		//mapSatisfaction.put("Insatisfação", BigDecimal.ONE.subtract(satisfaction.divide(satisfaction.add(dissatisfaction), MathContext.DECIMAL128)).multiply(new BigDecimal("5")));
		
		return mapSatisfaction;
		
	}
	
	private String getEvaluation(HttpServletRequest request) throws NumberFormatException, SQLException, ServletException {
		
		ResultSet rs = queryEvaluation(request);
		
		return gson.toJson(calcLIWCAnScore(rs).entrySet()
		        .stream()
		        .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
		        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new)));
	}
	

	private ResultSet queryEvaluation(HttpServletRequest request) throws SQLException {
		boolean onlyDesiredCategories = false;
		
		int userID = Integer.parseInt(request.getParameter("user_id"));
		int lessonID = Integer.parseInt(request.getParameter("lesson_id"));
		
		List<String> desiredCategoriesString = new ArrayList<>();
		
		if (request.getParameterValues("c[]") != null){
			desiredCategoriesString = Arrays.asList(request.getParameterValues("c[]"));
			onlyDesiredCategories = true;
		}
		
		String query = "SELECT c.name, ec.count, ec.anew_score " + 
						"FROM lesson_evaluation le " + 
						"JOIN evaluation_category ec ON ec.id_evaluation = le.id_evaluation " + 
						"JOIN category c ON c.id_category = ec.id_category " + 
						"WHERE le.id_lesson = ? AND le.id_user = ? ";
		
		if (onlyDesiredCategories)
			query = query.concat("AND c.liwc_id IN " + generateQueryArrayString(liwcHelper.fromStringToIntCategory(desiredCategoriesString)));
		
		PreparedStatement pStmt = MysqlCon.getConnection().prepareStatement(query);
		
		pStmt.setInt(1, lessonID);
		pStmt.setInt(2, userID);
		
		ResultSet rs = pStmt.executeQuery();
		return rs;
	}

	private <T> String generateQueryArrayString(List<T> array) {
		
		StringBuilder sb = new StringBuilder("(");
		
		for (int i = 0; i < array.size(); i++) {
			String value = array.get(i).toString();
			sb.append(i == 0 ? value : ", " + value);
		}
		
		return sb.append(')').toString();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	private void changeDictionaryLanguage(String language){
		dic = "e".equals(language) ? englishDic : portugueseDic;
	}
	
	private String getTempSatisfaction(String text) throws SQLException {
		Map<Integer, ScoreStructure> mapScoresStructure = getLiwcAnStructure(text, "p");		
		Map<String, BigDecimal> mapScores = calcLIWCAnScore(mapScoresStructure);
		
		return calcSatisfaction(mapScores).toString();
	}
}

