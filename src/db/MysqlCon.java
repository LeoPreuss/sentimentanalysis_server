package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import global.Common;

public class MysqlCon {
	
	private MysqlCon(){};
	
	private static Connection connection = null;
	
	public static Connection getConnection() {
		try {
			
			if (connection == null || connection.isClosed()) {
				Class.forName("com.mysql.jdbc.Driver");
				String url = String.format("jdbc:mysql://%s:3306/%s?useUnicode=true&character-encoding=UTF-8", Common.DB_HOST, Common.DB_NAME);
				
				connection = DriverManager.getConnection(url, Common.DB_USER_NAME, Common.DB_PASSWORD);
			}
			
		} catch(ClassNotFoundException e) {
			System.out.println("Driver JDBC n�o instalado ou n�o encontrado.");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar � URL de BD especificada ou timeout.");
			e.printStackTrace();
		}
		
		return connection;
	}
} 